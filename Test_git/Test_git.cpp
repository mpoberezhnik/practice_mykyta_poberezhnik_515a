/*
@ author Mykyta Poberezhnik
@ group 515a
@ date 6/19/2019
@ brief ������� � ��������
*/


#include <stdio.h>
#include <locale.h>
#include <math.h>
#include <limits.h>
void int_into_char(long long int b); //�������� ������� �� �������� ������ ����� � ����� �� ���������� �����

int main()
{
	setlocale(LC_ALL, "Ukrainian");
	long long int num = 0;
	printf("����i�� �i�� �����:  ");
	scanf("%lld", &num);
	long long int li = LONG_MAX;
	if (fabs(num) >= li) {
		printf("�������. ������������.\n");
		return 0;
	}
	printf("%lld -> ", num);
	int_into_char(num);


	return 0;
}

void int_into_char(long long int b)
{


	const char* wrd_1[] = { "", "����", "���", "���","������","����","�i���","�i�","�i�i�","������" };
	const char* wrd_2[] = { "������", "�����������", "����������", "����������", "������������",\
"�'���������", "�i���������", "�i��������", "�i�i��������", "���'���������" };
	const char* wrd_3[] = { "", "��������", "��������", "�����",\
			"�'�������", "�i�������", "�i������", "�i�i������",
			"���'������", "���" };
	const char* wrd_4[] = { "���", "��i��i", "������", "���������", "�'������",\
			"�i�����", "�i����", "�i�i����", "���'�����" };
	const char* wrd_5[] = { "������","�����i","�����" };
	const char* wrd_6[] = { "�i�����", "�i������", "�i�����i�" };
	const char* wrd_7[] = { "�i�����", "�i������" };
	const char* wrd_8[] = { "", "����", "��i", "���", "������", "�'���","�i���","�i�","�i�i�","������" };
	if (b == 0)
	{
		printf("����");
		return;
	}
	if (b > 0)
		printf("");
	else
		printf("�i��� ");
	b = fabs(b);
	if (b > 999999999)
	{
		printf("%s ", wrd_1[b / 1000000000]);
		printf("%s ", wrd_7[b / 1000000000 - 1]);
	}
	b = b % 1000000000;
	if (b > 999999)
	{
		if (b > 99999999)
			printf("%s ", wrd_4[b / 100000000 - 1]);
		b = b % 100000000;
		long int c = b / 1000000;
		if (b >= 20000000)
			printf("%s ", wrd_3[b / 10000000 - 1]);
		else if (b < 20000000 && b >= 10000000)
			printf("%s ", wrd_2[(b / 1000000) % 10]);
		b = b % 100000000;
		if (b < 10000000)
		{
			b = b % 10000000;
			printf("%s ", wrd_1[b / 1000000]);
		}
		b = b % 10000000;
		if ((c >= 10 && c <= 20) || c % 10 == 0 || (c % 10 >= 5 && c % 10 <= 9))
			printf("%s ", wrd_6[2]);
		else if (c % 10 == 1)
			printf("%s ", wrd_6[0]);
		else printf("%s ", wrd_6[1]);
	}
	b = b % 1000000;
	if (b > 999)
	{
		if (b > 99999)
			printf("%s ", wrd_4[b / 100000 - 1]);
		b = b % 100000;
		long int c = b / 1000;
		if (b >= 20000)
			printf("%s ", wrd_3[b / 10000 - 1]);
		else if (b < 20000 && b >= 10000)
			printf("%s ", wrd_2[(b / 1000) % 10]);
		else if (b > 999)
		{
			b = b % 10000;
			printf("%s ", wrd_8[b / 1000]);
		}
		b = b % 100000;
		if (b < 10000)
		{
			b = b % 10000;
			printf("%s ", wrd_1[b / 1000]);
		}
		if ((c >= 10 && c <= 20) || c % 10 == 0 || (c % 10 >= 5 && c % 10 <= 9))
			printf("%s ", wrd_5[2]);
		else if (c % 10 == 1)
			printf("%s ", wrd_5[0]);
		else printf("%s ", wrd_5[1]);
	}
	b = b % 1000;
	if (b > 0)
	{
		if (b > 99)
			printf("%s ", wrd_4[b / 100 - 1]);
		b = b % 100;
		if (b >= 20)
		{
			printf("%s ", wrd_3[b / 10 - 1]);
			if (b % 10 > 0)
			{
				printf("%s ", wrd_1[b % 10]);
			}
		}
		else if (b < 10)
			printf("%s ", wrd_1[b]);
		else if (b < 20 && b >= 10)
			printf("%s ", wrd_2[b % 10]);
	}
	return;
}